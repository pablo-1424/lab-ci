#ifndef __MONCODE_H
#define __MONCODE_H

#include "stdint.h"

int32_t integer_avg(int32_t a, int32_t b);

#endif //__MONCODE_H