#include "monCode.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_avg_number(void)
{
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 0), "Error in integer_sum");
	TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_avg(1, 1), "Error in integer_sum");
	TEST_ASSERT_EQUAL_INT_MESSAGE(3, integer_avg(2, 4), "Error in integer_sum");
	TEST_ASSERT_EQUAL_INT_MESSAGE(-1, integer_avg(-1, -1), "Error in integer_sum");
}